//MrPyro 2019
package com.github.parker8283.bon2.gui;

import java.awt.FileDialog;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.nio.file.Paths;

import javax.swing.JTextField;

import com.github.parker8283.bon2.BON2;
import com.github.parker8283.bon2.BON2Gui;

public class LinuxCacheBrowseListener extends MouseAdapter{
    private BON2Gui parent;
    private JTextField field;
    private FileDialog fd;

    public LinuxCacheBrowseListener(BON2Gui parent, JTextField field) {
        this.parent = parent;
        this.field = field;

        this.fd = new FileDialog(this.parent, "Please Select Cache Folder", FileDialog.LOAD);

        fd.setFilenameFilter( (dir, name) -> dir.isDirectory());

        String key = BON2Gui.PREFS_KEY_OPEN_LOC;
        String savedDir = parent.prefs.get(key, Paths.get("").toAbsolutePath().toString());
        File currentDir = new File(savedDir);

        if (!Paths.get(savedDir).getRoot().toFile().exists()) {
            currentDir = Paths.get("").toAbsolutePath().toFile();
        }

        while (!currentDir.isDirectory()) {
            currentDir = currentDir.getParentFile();
        }

        fd.setDirectory(currentDir.getPath());
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int returnState;

        fd.setVisible(true);

        returnState = fd.getFile() == null ? 0 : 1;

        if (returnState == 1) {
            File file = new File(fd.getDirectory() + fd.getFile());
            String path = fd.getDirectory() + fd.getFile();

            field.setText(path);

            String parentFolder = file.getParentFile().getAbsolutePath();

            parent.getOutputField().setText(path);
            parent.prefs.put(BON2Gui.PREFS_KEY_CACHE_LOC, parentFolder);

            BON2.cacheFileDirectory = file;
        }
    }
}
