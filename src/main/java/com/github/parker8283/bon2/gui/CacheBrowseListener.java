package com.github.parker8283.bon2.gui;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;

import javax.swing.JFileChooser;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;

import com.github.parker8283.bon2.BON2;
import com.github.parker8283.bon2.BON2Gui;

public class CacheBrowseListener extends MouseAdapter {
    private BON2Gui parent;
    private JTextField field;
    private JFileChooser fileChooser;

    public CacheBrowseListener(BON2Gui parent, JTextField field) {
        this.parent = parent;
        this.field = field;
        this.fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setFileHidingEnabled(false);
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                String fileName = f.getName();
                return f.isDirectory() || fileName.endsWith(".jar");
            }

            @Override
            public String getDescription() {
                return "Please select cache folder";
            }
        });
        String key = BON2Gui.PREFS_KEY_CACHE_LOC;
        String savedDir = parent.prefs.get(key, Paths.get("").toAbsolutePath().toString());
        File currentDir = new File(savedDir);
        if(!Paths.get(savedDir).getRoot().toFile().exists()){
            currentDir = Paths.get("").toAbsolutePath().toFile();
        }
        while (!currentDir.isDirectory()) {
            currentDir = currentDir.getParentFile();
        }
        fileChooser.setCurrentDirectory(currentDir);
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        int returnState = fileChooser.showOpenDialog(parent);

        if(returnState == JFileChooser.APPROVE_OPTION) {
            File file = fileChooser.getSelectedFile();
            String path = null;
            try {
                path = file.getCanonicalPath();
            } catch (IOException ex) {
                path = file.getAbsolutePath();
            }

            field.setText(path);

            String parentFolder = file.getParentFile().getAbsolutePath();
            parent.prefs.put(BON2Gui.PREFS_KEY_CACHE_LOC, parentFolder);

            BON2.cacheFileDirectory = file;
        }
    }
}
